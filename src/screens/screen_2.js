import React, { Component } from "react";
import { logger, transportFunctionType } from "react-native-logs";
import { FileLogger } from "react-native-file-logger";

FileLogger.configure();
// import { rnFsFileAsync } from "react-native-logs/dist/transports/rnFsFileAsync";
// import * as FileSystem from "expo-file-system";
// const log = logger.createLogger();
import RNFS from "react-native-fs";
import {
  Button,
  PermissionsAndroid,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
const customTransport = (msg, level, options) => {
  // Do here whatever you want with the log message
  // You cas use any options setted in config.transportOptions
  console.log(
    "logging data ==--",
    level?.text,
    "--------------------------------***********************************",
    msg
  );
};

const config = {
  transport: customTransport,
};

var log = logger.createLogger(config);

const requestStoragePermission = async () => {
  const loggingPath = await FileLogger.getLogFilePaths();
  console.log("loggingPath==----", loggingPath);
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      //   android.permission.WRITE_EXTERNAL_STORAGE
      //   'android.permission.CAMERA'
      {
        title: "Permisiion Allowance",
        message: "You are allowing the permission to store the file.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the Storage");
      // file:///data/user/0/com.loggerapp2/files/myFolder/text.txt
      // let fileUri = FileSystem.documentDirectory + "myFolder/text.txt";
      // let fileUri = "//data/user/0/com.loggerapp2/files/myFolder/text.txt";
      //   let fileUri = "/storage/" + "text.txt";
      // console.log("fileUri===-", fileUri);
      // console.log("DownLoadPath===-", DownLoadPath);
      // RNFS.writeAsStringAsync(DownLoadPath, "contents", {
      //   encoding: RNFS.EncodingType.UTF8,
      // });
      const DownLoadPath = RNFS.DownloadDirectoryPath + "/joke.txt";
      log.error("DownLoadPath==--", DownLoadPath);
      const fileContent =
        "Why do programmers wear glasses? \n They can't C#!  \n Hardikh  \n Chinmay";
      // const makeFile = async (filePath, content) => {
      try {
        //create a file at filePath. Write the content data to it
        await RNFS.writeFile(DownLoadPath, fileContent, "utf8");
        console.log("written to file");
        log.error("*****************written to file");
      } catch (error) {
        //if the function throws an error, log it out.
        console.log("error===----", error);
      }
      // };

      // makeFile(DownLoadPath, fileContent);
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.warn(err);
  }
};
class Storage extends Component {
  componentDidMount() {
    // console.log("Its Logging Suuceesfully fron screen -2");

    log.debug("This is a Debug log");
    // log.info("This is an Info log");
    // log.warn("This is a Warning log");
    // log.error("This is an Error log");
    // console.log("loggerrr value====", log);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.item}>Try permissions</Text>
        <Button
          title="request permissions"
          onPress={requestStoragePermission}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingTop: StatusBar.currentHeight,
    backgroundColor: "#ecf0f1",
    padding: 8,
  },
  item: {
    margin: 24,
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
});
export default Storage;
// data/data/com.termux/files/home
// sdk_gphone_x86
