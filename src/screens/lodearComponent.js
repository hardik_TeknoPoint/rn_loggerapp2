import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
// import { BorderlessButton } from "react-native-gesture-handler";
import Spinner from "react-native-loading-spinner-overlay";
import { ActivityIndicator } from "react-native";
// import Spinnerimg from "./Spinnerimg";
import Spinnerimg from "./loader_3";
import { SafeAreaView } from "react-native";
// import { bounce } from "react-native/Libraries/Animated/Easing";
// import { SafeAreaView } from "react-native-safe-area-context";
// import { Bubbles, DoubleBounce, Bars, Pulse } from 'react-native-loader';
// type Props = {};
export default class Loaderss extends Component {
  state = {
    spinner: false,
  };
  componentDidMount() {
    setInterval(() => {
      this.setState({
        spinner: !this.state.spinner,
      });
    }, 3000);
  }
  render() {
    return (
      <SafeAreaView>
        <View style={styles.container}>
          <Spinner
            // indicatorStyle={{}}
            // spinnerKey=''
            // size={10}
            // color="yellow"
            visible={this.state.spinner}
            customIndicator={<Spinnerimg />}
            // overlayColor={"red"}
            animation="fade"
            // textContent={'Loading...'}
            textStyle={styles.spinnerTextStyle}
          ></Spinner>
          {/* <Text style={styles.welcome}>Welcome to React Native!</Text>
                <Text style={styles.instructions}>To get started, edit App.js</Text>
                <Text style={styles.instructions}></Text> */}
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: "#FFF",
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5,
  },
  loaderColor: {
    color: "red",
  },
});
// import React, { Component } from 'react';
// import Button from '../components/Button';
// import { StyleSheet, View, Text } from 'react-native';
// import AnimatedLoader from 'react-native-animated-loader';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// export default class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = { visible: false };
//   }
//   handlePress = () => {
//     setTimeout(() => {
//       this.setState({
//         visible: !this.state.visible,
//       });
//     }, 1000);
//   };
//   render() {
//     const { visible } = this.state;
//     return (
//       <View style={styles.container}>
//         <AnimatedLoader
//           visible={visible}
//           overlayColor="rgba(255,255,255,0.75)"
//           animationStyle={styles.lottie}
//           speed={1}
//           source={require("../Assets/86995-mechanics-gears.json")}
//         />
//          <Button text={"Press"} onpress={this.handlePress}/>
//       </View>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   lottie: {
//     width: 100,
//     height: 100,
//   },
// });
