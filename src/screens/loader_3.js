import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Button,
  requireNativeComponent,
  Text,
  Image,
  Animated,
  TouchableOpacity,
  Easing,
} from "react-native";
class Spinnerimg extends React.Component {
  spinValue = new Animated.Value(0);
  // set (animation,loop({clock, duration:5000}))
  animation() {
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 5000,
      // spinValue:1,
      useNativeDriver: false,
    }).start();
  }
  //    loop(animation, config)
  componentDidMount() {
    this.animation();
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Animated.Image
          style={{
            height: 60,
            width: 60,
            transform: [
              {
                rotate: this.spinValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0deg", "360deg"],
                }),
              },
            ],
          }}
          source={require("../asstes/images/settings.png")}
        />
        <Animated.Image
          style={{
            marginLeft: 20,
            marginTop: -3,
            height: 30,
            width: 30,
            transform: [
              {
                rotate: this.spinValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["360deg", "0deg"],
                }),
              },
            ],
          }}
          source={require("../asstes/images/settings.png")}
        />
      </View>
    );
  }
}
export default Spinnerimg;
