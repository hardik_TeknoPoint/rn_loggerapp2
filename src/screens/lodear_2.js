import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Button,
  requireNativeComponent,
  Text,
  Image,
  Animated,
  TouchableOpacity,
  Easing,
} from "react-native";
class ImageSpinner extends React.Component {
  state = {
    // animation:false
  };
  // First set up animation
  // Animated.timing(spinValue, {
  //     toValue: { x: 100, y: 100 },
  //     duration: 3000,
  //     // easing: Easing.linear, // Easing is an additional import from react-native
  //     useNativeDriver: true  // To make use of native driver for performance
  // }).start()
  spinValue = new Animated.Value(0);
  anotherSpinValue = new Animated.Value(0);
  animation() {
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: false,
    }).start(),
      Animated.timing(this.anotherSpinValue, {
        toValue: 1,
        duration: 5000,
        useNativeDriver: false,
      }).start();
  }
  componentDidMount() {
    // setTimeout(()=>{this.setState({animationOn:!this.state.animationOn})},3000)
    setInterval(() => {
      this.setState(this.animation());
    }, 3000);
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Animated.Image
          visible={this.animation()}
          style={{
            height: 100,
            width: 100,
            backgroundColor: "red",
            transform: [
              {
                rotate: this.spinValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0deg", "360deg"],
                }),
              },
            ],
          }}
          source={require("../asstes/images/settings.png")}
        />
        <Animated.Image
          style={{
            height: 50,
            width: 50,
            backgroundColor: "red",
            marginLeft: 35,
            transform: [
              {
                rotate: this.anotherSpinValue.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["360deg", "0deg"],
                }),
              },
            ],
          }}
          source={require("../asstes/images/settings.png")}
        />
        <Text style={{ marginTop: 15, fontSize: 25 }}>Loading...</Text>
      </View>
    );
  }
}
export default ImageSpinner;
