import { Text, View, StyleSheet, Image, Animated, Modal } from "react-native";
import React, { Component } from "react";
import AnimatedImg from "../components/animatedImg";

export default class Loader extends Component {
  state = {
    showAnimation: true,
  };
  componentDidMount() {
    setInterval(() => {
      console.log("5000==--");
      this.setState((pre) => {
        return { showAnimation: !this.state.showAnimation };
      });
    }, 3000);
  }
  // componentDidMount() {
  // setInterval(() => {
  // this.setState((pre) => {
  //   console.log("5000==--", pre);
  //   return { showAnimation: !this.state.showAnimation };
  // });
  // this.makeAnimation();
  // }, 3000);
  // Animated.timing(this.animation1, { duration: 0 });
  // Animated.timing(this.animation2, { duration: 0 });
  // }

  // animation1 = new Animated.Value(0);
  // animation2 = new Animated.Value(0);
  // makeAnimation = () => {
  //   Animated.timing(this.animation1, {
  //     toValue: 1,
  //     duration: 5000,
  //     useNativeDriver: false,
  //   }).start();
  // };
  render() {
    return (
      <View style={styles.Loader}>
        {/* <Animated.Image
          // visible={this.state.showAnimation}
          style={{
            height: 65,
            width: 65,
            marginTop: -70,
            marginLeft: -40,
            transform: [
              {
                rotate: this.animation1.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["0deg", "360deg"],
                }),
              },
            ],
          }}
          source={require("../asstes/images/settings.png")}
        /> */}
        <AnimatedImg />
        <AnimatedImg />

        <Text style={{ fontSize: 20, fontWeight: "600" }}>
          Loading Data.....
        </Text>
        {/* <Modal visible={!this.state.showAnimation} style={styles.modelStyles}>
          <View style={styles.modelViewStyles}>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Showing_Data.....
            </Text>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Showing_Data.....
            </Text>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Showing_Data.....
            </Text>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Showing_Data.....
            </Text>
            <Text style={{ fontSize: 20, fontWeight: "600" }}>
              Showing_Data.....
            </Text>
          </View>
        </Modal> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Loader: {
    position: "relative",
    flex: 1,
    width: "100%",
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
  },
  modelStyles: {
    // flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modelViewStyles: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "green",
    width: "100%",
    height: "100%",
  },
  gear_1: {
    position: "absolute",
    top: "50%",
  },
  gear_2: {
    position: "absolute",
    top: "54%",
    left: "50%",
  },
});
