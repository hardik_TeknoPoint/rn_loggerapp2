import { Text, View, StyleSheet, Image, Animated, Modal } from "react-native";

import React, { Component } from "react";

export default class AnimatedImg extends Component {
  animation1 = new Animated.Value(0);

  componentDidMount() {
    console.log("makeAnimation");
    this.makeAnimation();
  }
  makeAnimation = () => {
    Animated.timing(this.animation1, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: false,
    }).start();
  };
  render() {
    return (
      <View>
        <Animated.Image
          // visible={this.state.showAnimation}
          style={{
            height: 45,
            width: 45,
            marginLeft: 20,
            marginTop: -10,
            transform: [
              {
                rotate: this.animation1.interpolate({
                  inputRange: [0, 1],
                  outputRange: ["360deg", "0deg"],
                }),
              },
            ],
          }}
          source={require("../asstes/images/settings.png")}
        />
      </View>
    );
  }
}
